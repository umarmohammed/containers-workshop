using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using networks_demo.Data;
using System;
using System.Linq;

namespace networks_demo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            if (args.Contains("recreatedb"))
            {
                RecreateDb(host);
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static void RecreateDb(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<EmployeesContext>();

                Console.WriteLine("Dropping database");
                db.Database.EnsureDeleted();
                Console.WriteLine("Migrating database");
                db.Database.Migrate();
                Console.WriteLine("Seeding database");
                SeedDb(db);
            }
        }

        private static void SeedDb(EmployeesContext employeesContext)
        {
            employeesContext.Employees.AddRange(SeedData.Employees);
            employeesContext.SaveChanges();
        }
    }
}
