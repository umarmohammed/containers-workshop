## Networks demo

This project demonstrates how to use docker networks. See https://docs.docker.com/network/ for more information.

### Create a network

```
docker network create --driver bridge my_net
```

### Run the postgres container on the network

```
docker run --net=my_net --name postgres -e POSTGRES_PASSWORD=postgres -d postgres
```

The `--name` option allows this container to be referenced by others containers as `postgres`.

### Run this demo project on the network

Build the image

```
docker build -t aspnet-network-demo .
```

Run the image

```
docker run --net=my_net -p 5000:5000 aspnet-network-demo recreatedb
```
