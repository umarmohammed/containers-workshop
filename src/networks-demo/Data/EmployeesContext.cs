﻿using Microsoft.EntityFrameworkCore;

namespace networks_demo.Data
{
    public class EmployeesContext : DbContext
    {
        public EmployeesContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
    }
}
