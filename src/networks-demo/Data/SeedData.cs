﻿using System.Collections.Generic;

namespace networks_demo.Data
{
    public class SeedData
    {
        public static IEnumerable<Employee> Employees => new List<Employee>
        {
           new Employee { Name = "james", JobTitle = "SALESMAN" },
            new Employee { Name = "neha", JobTitle = "SOFTWARE DEVELOPER" },
            new Employee { Name = "mary", JobTitle = "APPLICATION SUPPORT DEVELOPER" },
            new Employee { Name = "smith", JobTitle = "TESTER" },
            new Employee { Name = "chris", JobTitle = "ACCOUNTS MANAGER" },
            new Employee { Name = "barbara", JobTitle = "OPERATIONS ASSISTANT" },
            new Employee { Name = "mia", JobTitle = "OPERATIONS ASSISTANT" },
            new Employee { Name = "alex", JobTitle = "SALESMAN" }
        };
    }
}
