export interface Job {
  id: string;
  percentge: number;
  createdUtc: Date;
  processedUtc: Date;
  text: string;
}
