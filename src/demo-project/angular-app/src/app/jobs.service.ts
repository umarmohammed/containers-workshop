import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Job } from './job.model';
import { webSocket } from 'rxjs/webSocket';
import { tap, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const CONNECTED_ACTION = 'CONNECTED';
const JOB_UPDATE_ACTION = 'UPDATE';
const JOB_CREATE_ACTION = 'CREATE';

interface Action {
  type: string;
  payload: any;
}

class ConnectedAction implements Action {
  readonly type = CONNECTED_ACTION;
  constructor(public payload: Job[]) {}
}

class UpdateAction implements Action {
  readonly type = JOB_UPDATE_ACTION;
  constructor(public payload: Job) {}
}

class CreateAction implements Action {
  readonly type = JOB_CREATE_ACTION;
  constructor(public payload: Job) {}
}

type Actions = ConnectedAction | UpdateAction | CreateAction;

@Injectable({ providedIn: 'root' })
export class JobService {
  private subject = webSocket(`${environment.baseUrl}api/jobs`);

  private jobsSubject = new BehaviorSubject<Job[]>([]);

  jobs$ = this.subject.pipe(
    tap((action: Actions) =>
      this.jobsSubject.next(this.reducer(this.jobsSubject.value, action))
    ),
    switchMap(() => this.jobsSubject)
  );

  process(text: string) {
    this.subject.next(text);
  }

  private reducer(jobs: Job[], action: Actions): Job[] {
    switch (action.type) {
      case CONNECTED_ACTION: {
        return action.payload;
      }

      case JOB_UPDATE_ACTION: {
        return jobs.map(j => (j.id === action.payload.id ? action.payload : j));
      }

      case JOB_CREATE_ACTION: {
        return [action.payload, ...jobs];
      }

      default:
        return jobs;
    }
  }
}
