import { Component, OnInit } from '@angular/core';
import { webSocket } from 'rxjs/webSocket';

@Component({
  selector: 'app-root',
  template: `
    <app-jobs></app-jobs>
  `
})
export class AppComponent {}
