import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sentimentIcon' })
export class SentimentIconPipe implements PipeTransform {
  transform(value: number): string {
    if (value <= 20) {
      return '😡';
    }

    if (value <= 40) {
      return '🙁';
    }

    if (value <= 60) {
      return '😐';
    }

    if (value <= 80) {
      return '😃';
    }

    return '😍';
  }
}
