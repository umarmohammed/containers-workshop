import { Component } from '@angular/core';
import { JobService } from './jobs.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-jobs',
  template: `
    <mat-toolbar color="primary">
      <span>Container Workshop Demo</span>
    </mat-toolbar>

    <div class="table-container">
      <table mat-table [dataSource]="jobService.jobs$">
        <ng-container matColumnDef="id">
          <th class="id-col" mat-header-cell *matHeaderCellDef>Id</th>
          <td mat-cell *matCellDef="let element">{{ element.id }}</td>
        </ng-container>

        <ng-container matColumnDef="createdUtc">
          <th class="date-col" mat-header-cell *matHeaderCellDef>
            Created UTC
          </th>
          <td mat-cell *matCellDef="let element">
            {{ element.createdUtc | date: 'medium' }}
          </td>
        </ng-container>

        <ng-container matColumnDef="processedUtc">
          <th class="date-col" mat-header-cell *matHeaderCellDef>
            Processed UTC
          </th>
          <td mat-cell *matCellDef="let element">
            {{ element.processedUtc | date: 'medium' }}
          </td>
        </ng-container>

        <ng-container matColumnDef="text">
          <th mat-header-cell *matHeaderCellDef>Text</th>
          <td mat-cell *matCellDef="let element">
            {{ element.text }}
          </td>
        </ng-container>

        <ng-container matColumnDef="result">
          <th class="status-col" mat-header-cell *matHeaderCellDef>Result</th>

          <td mat-cell *matCellDef="let element">
            <span
              *ngIf="!!element.percentage && element.percentage >= 0"
              class="sentiment"
              >{{ element.percentage | sentimentIcon }}</span
            >
            <mat-spinner
              *ngIf="element.percentage === -1"
              [diameter]="30"
            ></mat-spinner>
          </td>
        </ng-container>

        <tr
          mat-header-row
          *matHeaderRowDef="displayedColumns; sticky: true"
        ></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
      </table>
    </div>

    <form (ngSubmit)="processText()">
      <input
        type="text"
        #text
        [formControl]="textControl"
        placeholder="type some text and press enter"
      />
    </form>
  `,
  styles: [
    `
      .table-container {
        height: 500px;
        overflow: auto;
      }
      button: {
        margin: 5px;
      }
      table {
        width: 100%;
      }
      .status-col {
        width: 75px;
      }

      .id-col {
        width: 300px;
      }

      .date-col {
        width: 180px;
      }
      input {
        width: 400px;
        margin: 10px;
      }
      .sentiment {
        font-size: 26px;
      }
    `
  ]
})
export class JobsComponent {
  displayedColumns = ['id', 'createdUtc', 'processedUtc', 'text', 'result'];

  textControl = this.fb.control('');

  constructor(public jobService: JobService, private fb: FormBuilder) {}

  processText() {
    this.jobService.process(this.textControl.value);
    this.textControl.reset();
  }
}
