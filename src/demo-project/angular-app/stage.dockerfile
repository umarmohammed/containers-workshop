FROM node:10 as build
WORKDIR /app
COPY package.json package.json
RUN npm install
COPY . .
RUN npm run build -- --configuration=staging

FROM nginx:alpine as publish
COPY --from=build /app/dist/angular-app /usr/share/nginx/html
