### build
FROM node:10 as build
WORKDIR /app
COPY package.json package.json
RUN npm install
COPY . .
RUN npm run build -- --prod

### runtime
FROM nginx:alpine
COPY --from=build /app/dist/angular-app /usr/share/nginx/html
