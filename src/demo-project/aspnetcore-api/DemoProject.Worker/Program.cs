﻿using DemoProject.Core.Jobs;
using DemoProject.Worker.Models;
using EasyNetQ;
using Microsoft.ML;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DemoProject.Worker
{
    class Program
    {
        static readonly ManualResetEvent QuitEvent = new ManualResetEvent(false);
        static PredictionEngine<SampleObservation, SamplePrediction> _engine;

        static async Task Main(string[] args)
        {
            Console.CancelKeyPress += (sender, eArgs) => {
                QuitEvent.Set();
                eArgs.Cancel = true;
            };

            var host = Environment.GetEnvironmentVariable("host");
            if (!string.IsNullOrEmpty(host))
            {
                var connectionString = $"host={host};prefetchcount=1";
                _engine = CreateEngine();
                using (var bus = RabbitHutch.CreateBus(connectionString))
                {
                    await Task.Run(() =>
                        bus.Subscribe<ProcessJob>("",
                            message => Process(message, bus)));

                    Console.WriteLine("Listening for messages. Hit ctrl+c to quit.");
                    QuitEvent.WaitOne();
                }
            }
        }

        private static void Process(ProcessJob message, IBus bus)
        {
            

            bus.Publish(Job.CreateProcessingJob(message));
            Console.WriteLine("Processing...");
            var percentage = Predict(message.Text);
            Thread.Sleep(3000);

            Console.WriteLine("done");
            bus.Publish(Job.CreateProcessedJob(message, percentage));
        }

        private static PredictionEngine<SampleObservation, SamplePrediction> CreateEngine()
        {
            var mlContext = new MLContext();
            ITransformer predictionPipeline = mlContext.Model.Load("MLSentimentModel.zip", out _);
            return mlContext.Model.CreatePredictionEngine<SampleObservation, SamplePrediction>(predictionPipeline);
        }

        public static float Predict(string sentimentText)
        {
            var sampleData = new SampleObservation() { Col0 = sentimentText };
            var prediction = _engine.Predict(sampleData);
            return CalculatePercentage(prediction.Score);
        }

        public static float CalculatePercentage(double value)
        {
            return 100 * (1.0f / (1.0f + (float)Math.Exp(-value)));
        }
    }
}
