﻿namespace DemoProject.Core.Events
{
    public interface IEventHandler<in T>
    {
        void Handle(T @event);
    }
}
