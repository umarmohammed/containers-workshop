﻿namespace DemoProject.Core.Events
{
    public interface IEventBus
    {
        void Publish<T>(T request) where T : class;

        void Subscribe<T>(IEventHandler<T> handler) where T : class;
        void SubscribeAsync<T>(IAsyncEventHandler<T> handler) where T : class;
    }
}
