﻿using System.Threading.Tasks;

namespace DemoProject.Core.Events
{
    public interface IAsyncEventHandler<in T>
    {
        Task Handle(T @event);
    }
}
