﻿using System.Text.Json;

namespace DemoProject.Core
{
    public static class ObjectExtensions
    {
        public static string ToJson(this object value)
        {
            return JsonSerializer.Serialize(
                value,
                new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                });
        }

        public static void SetPropertyValuesFrom<T>(this T obj, object fromObject)
        {
            foreach (var propertyInfo in fromObject.GetType().GetProperties())
            {
                if (propertyInfo.SetMethod != null)
                {
                    obj.SetPropertyValue(propertyInfo.Name, propertyInfo.GetValue(fromObject));
                }
            }
        }

        public static void SetPropertyValue<T>(this T obj, string propertyName, object value)
        {
            typeof(T).GetProperty(propertyName)?.SetValue(obj, value);
        }
    }
}
