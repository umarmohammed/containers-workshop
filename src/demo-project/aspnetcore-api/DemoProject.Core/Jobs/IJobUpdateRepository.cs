﻿namespace DemoProject.Core.Jobs
{
    public interface IJobUpdateRepository
    {
        void UpdateJob(Job job);
    }
}
