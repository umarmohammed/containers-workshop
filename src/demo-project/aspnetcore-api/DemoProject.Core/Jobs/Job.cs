﻿using System;

namespace DemoProject.Core.Jobs
{
    public class Job
    {
        public Guid Id { get; set; }
        public DateTime CreatedUtc { get; set; }
        public DateTime? ProcessedUtc { get; set; }
        public double? Percentage { get; set; }
        public string Text { get; set; }

        public static Job CreatePendingJob(string text)
        {
            return new Job
            {
                Id = Guid.NewGuid(),
                CreatedUtc = DateTime.UtcNow,
                Text = text
            };
        }

        public static Job CreateProcessingJob(ProcessJob processJob)
        {
            return new Job
            {
                Id = processJob.Id,
                Percentage = -1,
                CreatedUtc = processJob.CreatedUtc,
                Text = processJob.Text
            };
        }

        public static Job CreateProcessedJob(ProcessJob processJob, double percentage)
        {
            return new Job
            {
                Id = processJob.Id,
                Percentage = percentage,
                Text = processJob.Text,
                CreatedUtc = processJob.CreatedUtc,
                ProcessedUtc = DateTime.UtcNow
            };
        }
    }
}
