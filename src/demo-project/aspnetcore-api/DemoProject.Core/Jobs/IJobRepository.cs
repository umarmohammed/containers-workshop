﻿using System.Collections.Generic;

namespace DemoProject.Core.Jobs
{
    public interface IJobRepository
    {
        IEnumerable<Job> GetJobs();
        void AddJob(Job job);
    }
}
