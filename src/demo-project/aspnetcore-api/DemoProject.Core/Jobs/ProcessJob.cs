﻿using System;

namespace DemoProject.Core.Jobs
{
    public class ProcessJob
    {
        public Guid Id { get; set; }
        public DateTime CreatedUtc { get; set; }
        public string Text { get; set; }

        public static ProcessJob FromJob(Job job)
        {
            return new ProcessJob
            {
                Id = job.Id,
                CreatedUtc = job.CreatedUtc,
                Text = job.Text
            };
        }
    }
}
