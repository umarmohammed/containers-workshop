﻿using DemoProject.Core.Events;
using EasyNetQ;

namespace DemoProject.Infrastructure.EventBus
{
    public class RabbitMqEventBus : Core.Events.IEventBus
    {
        private readonly IBus _bus;

        public RabbitMqEventBus(IBus bus)
        {
            _bus = bus;
        }

        public void Publish<T>(T request) where T : class
        {
            _bus.Publish(request);
        }


        public void Subscribe<T>(IEventHandler<T> handler) where T : class
        {
            _bus.Subscribe<T>("", handler.Handle);
        }

        public void SubscribeAsync<T>(IAsyncEventHandler<T> handler) where T : class
        {
            _bus.SubscribeAsync<T>("", handler.Handle);
        }
    }
}
