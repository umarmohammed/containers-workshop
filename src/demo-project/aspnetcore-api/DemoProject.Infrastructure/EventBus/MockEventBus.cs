﻿using DemoProject.Core.Events;

namespace DemoProject.Infrastructure.EventBus
{
    public class MockEventBus : IEventBus
    {
        public void Publish<T>(T request) where T : class
        {
        }

        public void Subscribe<T>(IEventHandler<T> handler) where T : class
        {
        }

        public void SubscribeAsync<T>(IAsyncEventHandler<T> handler) where T : class
        {
        }
    }
}
