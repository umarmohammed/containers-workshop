﻿using DemoProject.Core;
using DemoProject.Core.Jobs;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Infrastructure.Persistence
{
    public class EfJobUpdateRepository : IJobUpdateRepository
    {
        private readonly DbContextOptions _options;

        public EfJobUpdateRepository(DbContextOptions options)
        {
            _options = options;
        }

        public void UpdateJob(Job job)
        {
            using (var db = new JobsContext(_options))
            {
                var existingJob = db.Jobs.Find(job.Id);
                if (existingJob == null) return;
                existingJob.SetPropertyValuesFrom(job);
                db.Entry(existingJob).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
