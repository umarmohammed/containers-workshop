﻿using DemoProject.Core.Jobs;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Infrastructure.Persistence
{
    public class JobsContext : DbContext
    {
        public JobsContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Job> Jobs { get; set; }
    }
}
