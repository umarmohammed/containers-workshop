﻿using DemoProject.Core.Jobs;

namespace DemoProject.Infrastructure.Persistence
{
    public class MockJobUpdateRepository : IJobUpdateRepository
    {
        public void UpdateJob(Job job)
        {
        }
    }
}
