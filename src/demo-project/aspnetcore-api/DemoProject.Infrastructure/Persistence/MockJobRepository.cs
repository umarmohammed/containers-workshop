﻿using DemoProject.Core.Jobs;
using System;
using System.Collections.Generic;

namespace DemoProject.Infrastructure.Persistence
{
    public class MockJobRepository : IJobRepository
    {
        private static List<Job> Jobs = new List<Job>();

        public void AddJob(Job job)
        {
            job.Id = Guid.Empty;
            Jobs.Add(job);
        }

        public IEnumerable<Job> GetJobs()
        {
            return Jobs;
        }
    }
}
