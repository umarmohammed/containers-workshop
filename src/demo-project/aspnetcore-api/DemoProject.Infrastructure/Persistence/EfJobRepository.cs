﻿using DemoProject.Core.Jobs;
using System.Collections.Generic;
using System.Linq;

namespace DemoProject.Infrastructure.Persistence
{
    public class EfJobRepository : IJobRepository
    {
        private readonly JobsContext _db;

        public EfJobRepository(JobsContext db)
        {
            _db = db;       
        }

        public void AddJob(Job job)
        {
            _db.Jobs.Add(job);
            _db.SaveChanges();
        }

        public IEnumerable<Job> GetJobs()
        {
            return _db.Jobs.Take(10).ToList();
        }
    }
}
