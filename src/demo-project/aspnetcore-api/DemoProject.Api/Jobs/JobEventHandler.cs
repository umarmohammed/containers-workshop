﻿using DemoProject.Api.WebSockets;
using DemoProject.Core;
using DemoProject.Core.Events;
using DemoProject.Core.Jobs;
using System.Threading.Tasks;

namespace DemoProject.Api.Jobs
{
    public class JobEventHandler : IAsyncEventHandler<Job>
    {
        private readonly WebSocketMessageSender _socketMessageSender;
        private readonly IJobUpdateRepository _jobRepository;

        public JobEventHandler(WebSocketMessageSender socketMessageSender, IJobUpdateRepository jobRepository)
        {
            _socketMessageSender = socketMessageSender;
            _jobRepository = jobRepository;
        }

        public async Task Handle(Job @event)
        {
            _jobRepository.UpdateJob(@event);
            await _socketMessageSender.SendMessageToAll(Action.Update(@event).ToJson());
        }
    }
}
