﻿using DemoProject.Core.Jobs;
using System.Collections.Generic;

namespace DemoProject.Api.Jobs
{
    public class Action
    {
        public string Type { get; private set; }
        public object Payload { get; private set; }

        private Action() { }

        public static Action Connected(IEnumerable<Job> jobs)
        {
            return new Action
            {
                Type = "CONNECTED",
                Payload = jobs
            };
        }

        public static Action Update(Job job)
        {
            return new Action
            {
                Type = "UPDATE",
                Payload = job
            };
        }

        public static Action Create(Job job)
        {
            return new Action
            {
                Type = "CREATE",
                Payload = job
            };
        }
    }
}
