﻿using DemoProject.Api.WebSockets;
using DemoProject.Core;
using DemoProject.Core.Events;
using DemoProject.Core.Jobs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace DemoProject.Api.Jobs
{
    public class JobController : ControllerBase
    {
        private readonly WebSocketConnectionManager _webSocketConnectionManager;
        private readonly IJobRepository _jobRepository;
        private readonly IEventBus _eventBus;
        private readonly WebSocketMessageSender _socketMessageSender;

        public JobController(
            WebSocketConnectionManager webSocketConnectionManager, 
            IJobRepository jobRepository, 
            IEventBus eventBus,
            WebSocketMessageSender socketMessageSender)
        {
            _webSocketConnectionManager = webSocketConnectionManager;
            _jobRepository = jobRepository;
            _eventBus = eventBus;
            _socketMessageSender = socketMessageSender;
        }

        [Route("api/jobs")]
        public async Task Jobs()
        {
            var context = ControllerContext.HttpContext;
            if (context.WebSockets.IsWebSocketRequest)
            {
                var webSocket = await context.WebSockets.AcceptWebSocketAsync();
                await OnConnected(webSocket);
                await Receive(webSocket, async (socket, data) => await HandleMessage(socket, data));
            }
            else
            {
                context.Response.StatusCode = 400;
            }
        }

        [Route("api/test")]
        public string Test()
        {
            return "Hello Containers Workshop";
        }

        private async Task HandleMessage(WebSocket socket, WebSocketData webSocketData)
        {
            await OnMessage(socket, webSocketData);

            if (webSocketData.IsClose)
            {
                await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                                statusDescription: "Closed by the WebSocketManager",
                                cancellationToken: CancellationToken.None);
                return;
            }
        }

        private async Task OnMessage(WebSocket socket, WebSocketData webSocketData)
        {
            if (webSocketData.Result.MessageType == WebSocketMessageType.Text)
            {
                var job = Job.CreatePendingJob(webSocketData.ToString());
                _jobRepository.AddJob(job);
                _eventBus.Publish(ProcessJob.FromJob(job));
                await _socketMessageSender.SendMessageToAll(Action.Create(job).ToJson());
            }

            if (webSocketData.IsClose)
            {
                _webSocketConnectionManager.RemoveSocket(socket);
            }
        }

        private async Task OnConnected(WebSocket webSocket)
        {
            _webSocketConnectionManager.AddSocket(webSocket);
            var message = Action.Connected(_jobRepository.GetJobs()).ToJson();
            await _socketMessageSender.SendMessageAsync(webSocket, message);
        }

        private async Task Receive(WebSocket socket, Action<WebSocket, WebSocketData> handleMessage)
        {
            var buffer = new byte[1024 * 4];

            while (socket.State == WebSocketState.Open)
            {
                var result = await socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer),
                                                       cancellationToken: CancellationToken.None);

                handleMessage(socket, new WebSocketData { Buffer = buffer, Result = result });
            }
        }
    }
}
