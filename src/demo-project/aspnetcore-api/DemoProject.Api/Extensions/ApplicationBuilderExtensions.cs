﻿using DemoProject.Api.Jobs;
using DemoProject.Core.Events;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace DemoProject.Api.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void ConfigureEventBus(this IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            var handler = app.ApplicationServices.GetRequiredService<JobEventHandler>();

            eventBus.SubscribeAsync(handler);
        }
    }
}
