﻿using DemoProject.Api.Jobs;
using DemoProject.Core.Jobs;
using DemoProject.Infrastructure.EventBus;
using DemoProject.Infrastructure.Persistence;
using EasyNetQ;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using IEventBus = DemoProject.Core.Events.IEventBus;

namespace DemoProject.Api.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddJobRepository(this IServiceCollection services, string connectionString)
        {
            Console.WriteLine($"dbConnectionString: {connectionString}");

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                services.AddSingleton<IJobUpdateRepository, MockJobUpdateRepository>();
                return services.AddScoped<IJobRepository, MockJobRepository>();
            }

            services.AddSingleton<IJobUpdateRepository, EfJobUpdateRepository>();
            services.AddSingleton<DbContextOptions>(oo =>
            {
                var optionsBuilder = new DbContextOptionsBuilder<JobsContext>();
                optionsBuilder.UseNpgsql(connectionString);
                return optionsBuilder.Options;
            });

            services.AddDbContext<JobsContext>(options => options.UseNpgsql(connectionString));
            return services.AddScoped<IJobRepository, EfJobRepository>();
        }

        public static IServiceCollection AddEventBus(this IServiceCollection services, string connectionString)
        {
            Console.WriteLine($"rabbitConnectionString: {connectionString}");

            services.AddSingleton<JobEventHandler>();

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                return services.AddSingleton<IEventBus, MockEventBus>();
            }

            services.AddSingleton(RabbitHutch.CreateBus(connectionString));
            return services.AddSingleton<IEventBus, RabbitMqEventBus>();
        }
    }
}
