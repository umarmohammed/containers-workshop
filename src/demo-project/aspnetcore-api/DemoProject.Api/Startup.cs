using DemoProject.Api.Extensions;
using DemoProject.Api.WebSockets;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DemoProject.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(); ;
            services.AddSingleton<WebSocketConnectionManager>();
            services.AddSingleton<WebSocketMessageSender>();
            services.AddJobRepository(Configuration.GetConnectionString("JobConnectionString"));
            services.AddEventBus(Configuration.GetConnectionString("RabbitMqConnectionString"));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseWebSockets();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.ConfigureEventBus();
        }
    }
}
