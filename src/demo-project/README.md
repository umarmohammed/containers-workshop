# Demo App

Live demo http://demo.umarmohammed.io/

Run postgres image

```
docker run --net=my_net --name postgres -e POSTGRES_PASSWORD=postgres -d postgres
```

Run rabbitmq image

```
docker run -d --hostname my-rabbit --name some-rabbit --net my_net -p 8081:15672 rabbitmq:3-management
```

Run api server image on network

```
docker run -e ConnectionStrings:JobConnectionString="User ID=postgres;Password=postgres;Host=postgres;Port=5432;Database=Employees;Pooling=true" -e ConnectionStrings:RabbitMqConnectionString="host=some-rabbit" -p 8080:80 --net my_net umarmohammed/demo-api recreatedb
```

Run worker image on network

```
docker run -e host=some-rabbit --net my_net -it umarmohammed/demo-worker
```

Run angular apps

```
docker run -p 8082:80 umarmohammed/demo-ng
```
